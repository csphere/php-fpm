## php-fpm

---

php-fpm基于centos镜像构建，使用Nginx实现Web,并结合php-fpm解析php

> Docker镜像是继承关系，所以构建一个完整的基础镜像非常重要，而在中间件镜像中只需要安装，配置运行产品的服务即可！

### 如何构建php-fpm镜像

```
git clone https://git.oschina.net/csphere/php-fpm.git
cd php-fpm
docker build -t csphere/php-fpm:5.4 .
```

### 创建容器

```
docker run -d -p 80:80 --name website --restart=always csphere/php-fpm:5.4
```

### ONBUILD

在中间件镜像中添加`ONBUILD`指令，可以非常快的构建出应用镜像。具体`ONBUILD`指令介绍请参考
git oschina[Dockerfile指令详解](http://t.cn/RU7Jlbf)
github [Dockerfile指令详解](https://github.com/billycyzhang/Shell/blob/master/Dockerfile%E5%91%BD%E4%BB%A4%E8%AF%A6%E8%A7%A3.md)
